<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@welcome')->name('welcome');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/like{post_id}', 'LikeController@like')->name('like');
Route::get('/dislike{post_id}', 'DislikeController@dislike')->name('dislike');

Route::get('/my-posts', 'PostController@posts')->name('my-posts');
Route::get('/videos', 'PostController@videos')->name('videos');
Route::get('/pictures', 'PostController@images')->name('images');

Route::get('/messages', 'MessageController@index')->name('messages');
Route::post('send-message', 'MessageController@sendMessage')->name('send-message');

Route::post('add-friend{id}', 'UserController@add')->name('add-friend');
Route::post('delete-request{id}', 'UserController@deleteRequest')->name('delete-request');
Route::post('confirm', 'FriendController@confirm')->name('confirm');
Route::post('delete-friend{id}', 'FriendController@destroy')->name('delete-friend');
Route::get('search', 'FriendController@search')->name('search');

Route::resource('posts', 'PostController', ['except' => ['create', 'edit', 'show']]);
Route::resource('comments', 'CommentController', ['except' => ['create', 'edit', 'show']]);
Route::resource('users', 'UserController', ['only' => ['edit', 'update', 'destroy']]);
Route::resource('likes', 'LikeController', ['only' => ['store']]);
Route::resource('dislikes', 'DislikeController', ['only' => ['store']]);
