<?php

namespace App\Http\Controllers;

use App\Events\PostStoreEvent;
use App\Friend;
use App\Http\Requests\PostRequest;
use App\Image;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function store(PostRequest $request)
    {
        $postStoreEvent = new PostStoreEvent();
        $postStoreEvent->setData($request);
        event($postStoreEvent);

        return redirect()->route('home');
    }

    public function welcome()
    {
        $posts = Post::with('image')->get();
        return view('welcome', compact('posts'));
    }

    public function posts()
    {
        $posts = Post::with('image')->where('user_id', auth()->user()->id)->get();
        $friends = Friend::with('user')->where('friend_id', auth()->user()->id)->orderByDesc('id')->get();
        return view('posts.posts', compact('posts', 'friends'));
    }

    public function videos()
    {
        $posts = Post::with('image', 'user')->whereNotNull('video')->get();
        return view('videos.videos', compact('posts'));
    }

    public function images()
    {
        $posts = Image::with('imageable', 'user')->get();
        $friends = Friend::with('user')->where('friend_id', auth()->user()->id)->orderByDesc('id')->get();
        return view('pictures.images', compact('posts', 'friends'));
    }
}
