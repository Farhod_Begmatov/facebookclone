<?php

namespace App\Http\Controllers;

use App\Friend;
use App\Post;
use App\RequestForFriend;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user_id = auth()->user()->id;
        $users = User::with('image')->where('id' , '!=', $user_id)->get();
        $posts = Post::with('image', 'user', 'likes', 'dislikes')->orderByDesc('id')->get();
        $friends = auth()->user()->friends();
        $requests = RequestForFriend::with('user')->where('friend_id', $user_id)->where('status', 0)->orderByDesc('id')->get();

        return view('home', compact(['posts', 'users', 'requests', 'friends']));
    }

}
