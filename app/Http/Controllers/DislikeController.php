<?php

namespace App\Http\Controllers;

use App\Dislike;
use App\Events\DislikeStoreEvent;
use App\Post;
use Illuminate\Http\Request;

class DislikeController extends Controller
{
    public function dislike($post_id)
    {
        $dislikeStoreEvent = new DislikeStoreEvent();
        $dislikeStoreEvent->setPostId($post_id);
        event($dislikeStoreEvent);
        return redirect()->route('home');
    }

}
