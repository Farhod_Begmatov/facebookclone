<?php

namespace App\Http\Controllers;

use App\Events\SendRequestForFriendEvent;
use App\Events\UpdateUserEvent;
use App\Http\Requests\UpdateUserRequest;
use App\RequestForFriend;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function edit(User $user)
    {
        return view('users.profile', ['user' => $user]);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $userUpdateEvent = new UpdateUserEvent();
        $userUpdateEvent->setRequest($request);
        $userUpdateEvent->setUser($user);
        event($userUpdateEvent);

        return redirect()->route('home');
    }

    public function add($id)
    {
        $sendRequestForFriend = new SendRequestForFriendEvent();
        $sendRequestForFriend->setId($id);
        event($sendRequestForFriend);
        return redirect()->back();
    }

    public function deleteRequest($id)
    {
        $request = RequestForFriend::find($id);
        $request->delete();
        return redirect()->back();
    }
}
