<?php

namespace App\Http\Controllers;

use App\Events\ConfirmFriendEvent;
use App\Friend;
use App\Post;
use App\RequestForFriend;
use App\User;
use Illuminate\Http\Request;

class FriendController extends Controller
{
    public function index()
    {
        $friends = Friend::with('user')
            ->where('friend_id' , auth()->user()->id)
            ->orderByDesc('id')
            ->get();
        return view('friends.friends', ['friends' => $friends]);
    }
    public function confirm(Request $request)
    {
        $confirmFriendEvent = new ConfirmFriendEvent();
        $confirmFriendEvent->setRequest($request);
        event($confirmFriendEvent);
        return redirect()->back();
    }

    public function destroy($id)
    {
        $friend = Friend::findOrFail($id)->first();
        if ($friend)
            $friend->delete();
        return redirect()->back();
    }

}
