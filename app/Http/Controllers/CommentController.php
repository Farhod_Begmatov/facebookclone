<?php

namespace App\Http\Controllers;

use App\Events\CommentStoreEvent;
use App\Lesson;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $commentEvent = new CommentStoreEvent();
        $commentEvent->setData($request->all());
        event($commentEvent);
        return redirect()->back();
    }

    public function destroy($id)
    {

    }
}
