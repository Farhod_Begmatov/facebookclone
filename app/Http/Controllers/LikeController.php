<?php

namespace App\Http\Controllers;

use App\Events\LikeStoreEvent;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function like($post_id)
    {
        $likeStoreEvent = new LikeStoreEvent();
        $likeStoreEvent->setPostId($post_id);
        event($likeStoreEvent);

        return redirect()->route('home');
    }

    public function update(Request $request, $id)
    {
        //
    }
}
