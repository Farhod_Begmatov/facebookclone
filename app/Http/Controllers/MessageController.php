<?php

namespace App\Http\Controllers;

use App\Events\SendMessageEvent;
use App\Friend;
use App\Http\Requests\MessageRequest;
use App\Message;
use App\RequestForFriend;
use App\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user_id = auth()->user()->id;
        $users = User::with('image')->where('id' , '!=', $user_id)->get();
        $messages = Message::where('user_id', $user_id)->get();
        $requests = RequestForFriend::with('user')->where('friend_id', $user_id)->where('status', 0)->orderByDesc('id')->get();
        $friends = auth()->user()->friends();
        return view('users.messages', compact('users', 'messages', 'requests', 'friends'));
    }

    public function sendMessage(MessageRequest $request)
    {
        $data = $request->data;

        $sendMessageEvent = new SendMessageEvent();
        $sendMessageEvent->setData($data);
        event($sendMessageEvent);

        return redirect()->back();
    }
}
