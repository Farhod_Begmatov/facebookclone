<?php

namespace App\Listeners;

use App\Events\LikeStoreEvent;
use App\Post;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LikeStoreListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(LikeStoreEvent $event)
    {
        $post_id = $event->getPostId();
        $post = Post::find($post_id);
        if (!$post)
            return redirect()->back();
        if (auth()->user()->hasLikedPost($post))
            return redirect()->back();
        if (auth()->user()->hasDislikedPost($post))
            return redirect()->back();
        $post->likes()->create([
            'user_id' => auth()->user()->id,
            'likeable_id' => $post_id,
            'likeable_type' => get_class($post)
        ]);
    }
}
