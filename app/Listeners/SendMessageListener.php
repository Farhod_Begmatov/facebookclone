<?php

namespace App\Listeners;

use App\Events\SendMessageEvent;
use App\Message;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendMessageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SendMessageEvent $event)
    {
        $data = $event->getData();
        foreach($data as $value){
            $user_id = $value['user_id'];
            $friend_id = $value['friend_id'];
            $message = $value['message'];
        }
        Message::create([
            'user_id' => $user_id,
            'friend_id' => $friend_id,
            'message' => $message
        ]);
    }
}
