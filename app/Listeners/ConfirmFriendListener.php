<?php

namespace App\Listeners;

use App\Events\ConfirmFriendEvent;
use App\Friend;
use App\RequestForFriend;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ConfirmFriendListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ConfirmFriendEvent $event)
    {
        $request = $event->getRequest();
        $requestForFriend = RequestForFriend::find($request['request_id']);
        Friend::create([
            'user_id' => $requestForFriend->user_id,
            'friend_id' => $requestForFriend->friend_id,
            'confirm' => 1
        ]);
        $requestForFriend->update([
            'status' => 1
        ]);
    }
}
