<?php

namespace App\Listeners;

use App\Events\UpdateUserEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UpdateUserListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UpdateUserEvent $event)
    {
        $request = $event->getRequest();
        $user = $event->getUser();
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'password_confirmation' => Hash::make($request->password_confirmation),
        ]);

        if (isset($request->image)){
            $user->image()->create([
                'image' => $request->image->store('users/images', 'public')
            ]);
        }
        if (isset($user->image)) {
            if (Storage::disk('public')->exists($user->image)){
                Storage::disk('public')->delete($user->image);
            }
        }
    }
}
