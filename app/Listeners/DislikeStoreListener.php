<?php

namespace App\Listeners;

use App\Events\DislikeStoreEvent;
use App\Post;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DislikeStoreListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(DislikeStoreEvent $event)
    {
        $post_id = $event->getPostId();
        $post = Post::find($post_id);
        if (!$post)
            return redirect()->back();
        if (auth()->user()->hasDislikedPost($post))
            return redirect()->back();
        if (auth()->user()->hasLikedPost($post))
            return redirect()->back();
        $post->dislikes()->create([
            'user_id' => auth()->user()->id,
            'dislikeable_id' => $post_id,
            'dislikeable_type' => get_class($post)
        ]);
    }
}
