<?php

namespace App\Listeners;

use App\Events\PostStoreEvent;
use App\Post;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PostStoreListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(PostStoreEvent $event)
    {
        $request = $event->getData();
        $post = Post::create([
            'user_id' => auth()->user()->id,
            'content' => $request->content,
        ]);
        if (isset($request->image))
            $post->image()->create([
                'image' => $request->image->store('posts', 'public'),
            ]);
    }
}
