<?php

namespace App\Listeners;

use App\Events\SendRequestForFriendEvent;
use App\RequestForFriend;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendRequestForFriendListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SendRequestForFriendEvent $event)
    {
        $id = $event->getId();
        $user_id = auth()->user()->id;
        $userfriend = $user_id.$id;
        $requests = RequestForFriend::query()->where('userfriend', $userfriend)->orWhere('userfriend', strrev($userfriend))->get();
        if (!count($requests)){
            RequestForFriend::create([
                'user_id' => $user_id,
                'friend_id' => $id,
                'userfriend' => $userfriend
            ]);
        }
    }
}
