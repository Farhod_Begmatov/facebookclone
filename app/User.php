<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }

    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    public function dislikes()
    {
        return $this->hasMany('App\Dislike');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function hasLikedPost(Post $post)
    {
        return $post->likes()
            ->where('likeable_id', $post->id)
            ->where('likeable_type', get_class($post))
            ->where('user_id', $this->id)
            ->count();
    }

    public function hasDislikedPost(Post $post)
    {
        return $post->dislikes()
            ->where('dislikeable_id', $post->id)
            ->where('dislikeable_type', get_class($post))
            ->where('user_id', $this->id)
            ->count();
    }

    public function friendsOfMine()
    {
        return $this->belongsToMany('App\User', 'friends', 'user_id', 'friend_id');
    }

    public function friendOf()
    {
        return $this->belongsToMany('App\User', 'friends','friend_id', 'user_id');
    }

    public function friends()
    {
        return $this->friendsOfMine()->wherePivot('confirm', 1)->get()->merge($this->friendOf()->wherePivot('confirm', 1)->get());
    }

}
