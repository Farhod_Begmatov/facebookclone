<?php

namespace App\Providers;

use App\Events\CommentStoreEvent;
use App\Events\ConfirmFriendEvent;
use App\Events\DislikeStoreEvent;
use App\Events\LikeStoreEvent;
use App\Events\PostStoreEvent;
use App\Events\SendMessageEvent;
use App\Events\SendRequestForFriendEvent;
use App\Events\UpdateUserEvent;
use App\Listeners\CommentStoreListener;
use App\Listeners\ConfirmFriendListener;
use App\Listeners\DislikeStoreListener;
use App\Listeners\LikeStoreListener;
use App\Listeners\PostStoreListener;
use App\Listeners\SendMessageListener;
use App\Listeners\SendRequestForFriendListener;
use App\Listeners\UpdateUserListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        UpdateUserEvent::class => [
            UpdateUserListener::class,
        ],

        ConfirmFriendEvent::class => [
            ConfirmFriendListener::class,
        ],

        CommentStoreEvent::class => [
            CommentStoreListener::class,
        ],

        PostStoreEvent::class => [
            PostStoreListener::class,
        ],

        LikeStoreEvent::class => [
            LikeStoreListener::class,
        ],

        DislikeStoreEvent::class => [
            DislikeStoreListener::class,
        ],

        SendMessageEvent::class => [
            SendMessageListener::class,
        ],

        SendRequestForFriendEvent::class => [
            SendRequestForFriendListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
