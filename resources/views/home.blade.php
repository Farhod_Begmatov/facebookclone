@extends('layouts.main')

@section('content')
    <div class="theme-layout">
        <section>
            <div class="gap gray-bg">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row" id="page-contents">
                                @include('includes.sidebar')
                                @include('includes.content')
                                @include('includes.mypage')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection


