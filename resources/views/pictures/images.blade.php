@extends('layouts.main')

@section('content')
    <div class="theme-layout">
        <section>
            <div class="gap gray-bg">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row" id="page-contents">
                                    @include('includes.sidebar')
                                <div class="col-lg-8">
                                    @foreach($posts as $post)
                                            <div class="central-meta item">
                                                <div class="user-post">
                                                    <div class="friend-info">
                                                        @if($post->user)
                                                            @if($post->imageable->user->image)
                                                            <figure>
                                                                <img src="storage/{{ $post->imageable->user->image->image }}" alt="" width="48px">
                                                            </figure>
                                                            @else
                                                                <figure>
                                                                    <img src="{{ asset('images/resources/friend-avatar5.jpg') }}" alt="" width="48px">
                                                                </figure>
                                                            @endif
                                                        <div class="friend-name mr-3">
                                                            <p>{{ $post->imageable->user->name }}</p>
                                                            <span>published: june,2 2018 19:PM</span>
                                                        </div>
                                                        @endif
                                                        <div class="post-meta">
                                                                <img src="storage/{{ $post->image}}" alt="">
                                                            <div class="we-video-info">
                                                                <ul>
                                                                    <li>
                                                                    <span class="comment" data-toggle="tooltip" title="Comments">
                                                                        <i class="fa fa-comments-o"></i>
                                                                        @if($post->comments)
                                                                            <ins>{{ count($post->comments) }}</ins>
                                                                        @endif
                                                                    </span>
                                                                    </li>
                                                                    <li>
                                                                    <span class="like" data-toggle="tooltip" title="like">
                                                                        <i class="ti-heart"></i>
                                                                        <ins>2.2k</ins>
                                                                    </span>
                                                                    </li>
                                                                    <li>
                                                                    <span class="dislike" data-toggle="tooltip" title="dislike">
                                                                        <i class="ti-heart-broken"></i>
                                                                        <ins>200</ins>
                                                                    </span>
                                                                    </li>
                                                                    <li class="social-media">
                                                                        <div class="menu">
                                                                            <div class="btn trigger"><i class="fa fa-share-alt"></i></div>
                                                                            <div class="rotater">
                                                                                <div class="btn btn-icon"><a href="#" title=""><i class="fa fa-html5"></i></a></div>
                                                                            </div>
                                                                            <div class="rotater">
                                                                                <div class="btn btn-icon"><a href="#" title=""><i class="fa fa-facebook"></i></a></div>
                                                                            </div>
                                                                            <div class="rotater">
                                                                                <div class="btn btn-icon"><a href="#" title=""><i class="fa fa-google-plus"></i></a></div>
                                                                            </div>
                                                                            <div class="rotater">
                                                                                <div class="btn btn-icon"><a href="#" title=""><i class="fa fa-twitter"></i></a></div>
                                                                            </div>
                                                                            <div class="rotater">
                                                                                <div class="btn btn-icon"><a href="#" title=""><i class="fa fa-css3"></i></a></div>
                                                                            </div>
                                                                            <div class="rotater">
                                                                                <div class="btn btn-icon"><a href="#" title=""><i class="fa fa-instagram"></i></a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="rotater">
                                                                                <div class="btn btn-icon"><a href="#" title=""><i class="fa fa-dribbble"></i></a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="rotater">
                                                                                <div class="btn btn-icon"><a href="#" title=""><i class="fa fa-pinterest"></i></a>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="description">
                                                                <p>
                                                                    {{ $post->content }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach
                                </div><!-- centerl meta -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@section('script')
    <script>
        var like = 0;
        var dislike = 0;

        document.getElementById("like").innerText = like;
        document.getElementById("dislike").innerText = dislike;

        function likeCounter() {
            like = like + 1;
            document.getElementById("like").innerText = like;
        }
        function dislikeCounter() {
            dislike = dislike + 1;
            document.getElementById("dislike").innerText = dislike;
        }

    </script>
@endsection


