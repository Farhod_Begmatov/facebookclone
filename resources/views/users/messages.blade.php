@extends('layouts.main')

@section('content')
    <div class="theme-layout">
        <section>
            <div class="gap gray-bg">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row" id="page-contents">
                                @include('includes.sidebar')
                                <div class="col-lg-6">
                                    <div class="widget friend-list stick-widget">
                                        <h4 class="widget-title">Messages</h4>
                                        <ul id="people-list" class="friendz-list">
                                            @foreach($users as $user)
                                                <li class="mt-5">
                                                    <div class="col-lg-6">
                                                        <aside class="sidebar static">
                                                            <div class="widget">
                                                            @foreach($user->messages as $message)
                                                                    <p class="m-2">{{ $message->message }}</p>
                                                            @endforeach
                                                            </div>
                                                        </aside>
                                                    </div>
                                                    @if($user->image)
                                                        <figure>
                                                            <a href="#" title=""><img src="storage/{{ $user->image->image }}" alt="" width="48px;"></a>
                                                            <p style="display: contents">{{ $user->name }}</p>
                                                        </figure>
                                                    @else
                                                        <figure>
                                                            <a href="#" title=""><img src="images/resources/friend-avatar6.jpg" alt=""></a>
                                                            <p style="display: contents">{{ $user->name }}</p>
                                                        </figure>
                                                    @endif
                                                </li>
                                                <div class="friendz-meta mt-2 float-right">
                                                    <div class="newpst-input">
                                                        <div class="col-lg-6">
                                                            <aside class="sidebar static">
                                                                <div class="widget">
                                                                    @foreach(auth()->user()->messages as $message)
                                                                        <p class="m-2">{{ $message->message }}</p>
                                                                    @endforeach
                                                                </div>
                                                            </aside>
                                                        </div>
                                                        @if(auth()->user()->image)
                                                            <figure class="float-right">
                                                                <a href="#" title=""><img src="storage/{{ auth()->user()->image->image }}" alt=""></a>
                                                                <p style="display: contents">{{ auth()->user()->name }}</p>
                                                            </figure>
                                                        @else
                                                                <figure class="float-right">
                                                                    <a href="#" title=""><img src="images/resources/friend-avatar5.jpg" alt=""></a>
                                                                    <p style="display: contents">{{ auth()->user()->name }}</p>
                                                                </figure>
                                                        @endif
                                                        <form method="POST" action="{{ route('send-message') }}" enctype="multipart/form-data">
                                                            @csrf
                                                            @method('POST')
                                                            <input type="hidden" name="data[{{ $loop->index }}][friend_id]" value="{{ $user->id }}">
                                                            <input type="hidden" name="data[{{ $loop->index }}][user_id]" value="{{ auth()->user()->id }}">
                                                            <textarea rows="2" placeholder="write something" name="data[{{ $loop->index }}][message]"></textarea>
                                                            <button type="submit" class="btn-sm mt-2 float-right">Send Message</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </ul>
                                    </div><!-- friends list sidebar -->
                                </div><!-- centerl meta -->
                                @include('includes.mypage')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection



