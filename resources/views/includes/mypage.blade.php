<div class="col-lg-3">
    <aside class="sidebar static">
        <div class="widget">
            <h4 class="widget-title">New Users</h4>
            <ul id="people-list" class="friendz-list">
                @foreach($users as $user)
                <li>
                    <a class="dropdown-item" href="{{ route('add-friend', $user->id) }}"
                       onclick="event.preventDefault();
                                                 document.getElementById('form').submit();">
                    </a>
                </li>
                    @if($user->image)
                        <figure>
                            <a href="#" title=""><img src="storage/{{ $user->image->image }}" alt=""></a>
                        </figure>
                    @else
                        <figure>
                            <a href="#" title=""><img src="{{ asset('images/resources/friend-avatar6.jpg') }}" alt=""></a>
                        </figure>
                    @endif
                    <div class="friendz-meta">
                        <p style="display: contents">{{ $user->name }}</p>
                        <div style="float: right">
                            <form action="{{ route('add-friend', $user->id) }}" style="display: contents" method="POST" enctype="multipart/form-data" id="form">
                                @csrf
                                @method('POST')
                                <button type="submit" class="btn-sm btn-primary">Add</button>
                            </form>
                            <form style="display: contents">
                                <button type="submit" class="btn-sm btn-primary">Delete</button>
                            </form>
                        </div>
                    </div>
                @endforeach
            </ul>
        </div><!-- all users sidebar -->
        <div class="widget friend-list stick-widget">
                <h4 class="widget-title">Friend Requests</h4>
                <div class="tab-pane" id="frends-req">
                <ul class="nearby-contct">
                    @foreach($requests as $request)
                        <div class="nearly-pepls ml-4">
                            @if(!$request->user->image)
                                <figure>
                                    <a href="#" title=""><img src="{{ asset('images/resources/friend-avatar6.jpg') }}" alt=""></a>
                                    <a href="#" title="" style="display: contents">{{ $request->user->name }}</a>
                                </figure>
                            @else
                                <figure>
                                    <a href="#" title=""><img src="storage/{{ $request->user->image->image }}" alt=""></a>
                                    <a href="#" title="" style="display: contents">{{ $request->user->name }}</a>
                                </figure>
                            @endif
                            <div class="friendz-meta" style="display: contents">
                                <form method="POST" style="display: contents;" action="{{ route('confirm') }}" id="form-confirm">
                                    @csrf
                                    <input type="hidden" name="request_id" value="{{ $request->id }}">
                                    <button type="submit" class="btn-sm btn-success" data-ripple="">Accept</button>
                                </form>
                                <form style="display: contents;" action="{{ route('delete-request', $request->id) }}" method="POST">
                                    @csrf
                                    <button class="btn-sm btn-danger" data-ripple="">Cancel</button>
                                </form>
                            </div>
                            <li>
                                <a class="dropdown-item" href="{{ route('confirm')}}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('form-confirm').submit();">
                                </a>
                            </li>
                        </div>
                    @endforeach
                </ul>
            </div>
        </div><!-- friends requests sidebar -->

    </aside>
</div><!-- sidebar -->
