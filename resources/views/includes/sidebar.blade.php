<div class="col-lg-3">
    <aside class="sidebar static">
        <div class="widget">
            <h4 class="widget-title">Profile</h4>
            <div class="your-page">
                @if(auth()->user()->image)
                    <figure>
                        <a href="#" title=""><img src="storage/{{ auth()->user()->image->image }}" alt=""></a>
                    </figure>
                @else
                    <figure>
                        <a href="#" title=""><img src="images/resources/friend-avatar5.jpg" alt=""></a>
                    </figure>
                @endif
                <div class="page-meta">
                    <a href="#" class="underline">{{ auth()->user()->name }}</a>
                </div>
            </div>
            <ul class="naves mt-3">
                <li>
                    <i class="ti-clipboard"></i>
                    <a href="{{ route('home') }}" title="">Home</a>
                </li>
                <li>
                    <i class="ti-files"></i>
                    <a href="{{ route('my-posts') }}" title="">My posts</a>
                <li>
                    <i class="ti-image"></i>
                    <a href="{{ route('images') }}" title="">images</a>
                </li>
                <li>
                    <i class="ti-comments-smiley"></i>
                    <a href="{{ route('messages') }}" title="">Messages</a>
                </li>
            </ul>
        </div><!-- Shortcuts -->
        <div class="widget friend-list stick-widget">
            <h4 class="widget-title">Friends</h4>
            <ul id="people-list" class="friendz-list">
                @if(!count($friends))
                    <p>You have no friends</p>
                @else
                    @foreach($friends as $friend)
                        <li>
                            <a class="dropdown-item" href="{{ route('delete-friend', $friend->id) }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('cancel').submit();">
                            </a>
                        </li>
                        @if($friend->image)
                            <figure>
                                <a href="#" title=""><img src="storage/{{ $friend->image->image}}" alt=""></a>
                            </figure>
                        @else
                            <figure>
                                <a href="#" title=""><img src="{{ asset('images/resources/friend-avatar6.jpg') }}" alt=""></a>
                            </figure>
                        @endif
                        <div class="friendz-meta">
                            <p style="display: contents">{{ $friend->name}}</p>
                            <div style="float: right">
                                <form action="{{ route('delete-friend', $friend->id) }}" style="display: contents" method="POST" enctype="multipart/form-data" id="cancel">
                                    @csrf
                                    @method('POST')
                                    <button type="submit" class="btn-sm btn-primary">Delete</button>
                                </form>
                            </div>
                        </div>
                    @endforeach
                @endif
            </ul>
        </div><!-- friends list sidebar -->
    </aside>
</div><!-- sidebar -->
