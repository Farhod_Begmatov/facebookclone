<div class="col-lg-6">
    @include('includes.post')
    @foreach($posts as $post)
        <div class="central-meta item">
            <div class="user-post">
                <div class="friend-info">
                    @if(isset($post->user->image))
                        <figure>
                            <img src="storage/{{ $post->user->image->image }}" alt="">
                        </figure>
                    @else
                        <figure>
                            <img src="{{ asset('images/resources/friend-avatar5.jpg') }}" alt="">
                        </figure>
                    @endif
                    <div class="friend-name">
                        <p>{{ $post->user->name }}</p>
                        <span>{{ $post->user->created_at }}</span>
                    </div>
                    <div class="post-meta">
                        @if(isset($post->image))
                            <img src="storage/{{ $post->image->image }}" alt="">
                        @elseif (isset($post->video))
                            <video width="875" height="500" controls>
                                <source src="storage/{{ $post->video }}">
                            </video>
                        @endif
                        <div class="we-video-info">
                            <ul>
                                <li>
                                    <span class="comment" data-toggle="tooltip" title="Comments">
                                        <i class="fa fa-comments-o"></i>
                                        <ins>{{ count($post->comments) }}</ins>
                                    </span>
                                </li>
                                <li>
                                    <span class="like" data-toggle="tooltip" title="like">
                                        <a href="{{ route('like', $post->id) }}">
                                            <i  class="ti-heart"></i>
                                            <ins>{{ count($post->likes) }}</ins>
                                        </a>
                                    </span>

                                </li>
                                <li>
                                    <span class="dislike" data-toggle="tooltip" title="dislike">
                                        <a href="{{ route('dislike', $post->id) }}">
                                            <i  class="ti-heart-broken"></i>
                                            <ins>{{ count($post->dislikes) }}</ins>
                                        </a>
                                    </span>

                                </li>
{{--                                <li>--}}
{{--                                    <span class="dislike" data-toggle="tooltip" title="dislike">--}}
{{--                                        <button onclick="dislikeCounter()" class="ti-heart-broken"></button>--}}
{{--                                    </span>--}}
{{--                                    <ins id="dislike" class="mb-2"></ins>--}}
{{--                                </li>--}}
                            </ul>
                        </div>
                        <div class="description">
                            <p>
                                {{ $post->content }}
                            </p>
                        </div>
                        <form action="{{ route('comments.store') }}" method="POST">
                            @csrf
                            @method('POST')
                            <div class="mb-3">
                                <input type="hidden" name="postId" id="postId" value="{{ $post->id }}">
                                <label for="body">Comments</label> {{ count($post->comments) }}
                                @foreach($post->comments as $comment)
                                    <p>&nbsp;
                                        <span class="font-weight-bold">{{ $comment->user->name }}</span>: {{ $comment->comment }}
                                    </p>
                                @endforeach
                                <textarea class="form-control" name="body" id="body"
                                          style=" min-width: 500px;"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Comment</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div><!-- centerl meta -->
