<div class="central-meta">
    <div class="new-postbox">
        @if(auth()->user()->image)
            <figure class="float-left">
                <a href="#" title=""><img src="storage/{{ auth()->user()->image->image }}" alt=""></a>
                <p style="display: contents">{{ auth()->user()->name  }}</p>
            </figure>
        @else
            <figure class="float-right">
                <a href="#" title=""><img src="images/resources/friend-avatar5.jpg" alt=""></a>
                <p style="display: contents">{{ auth()->user()->name }}</p>
            </figure>
        @endif
        <div class="newpst-input">
            <form method="POST" action="{{ route('posts.store') }}" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <textarea rows="2" placeholder="write something" name="content"></textarea>
                <div class="attachments">
                    <ul>
                        <li>
                            <i class="fa fa-image"></i>
                            <label class="fileContainer" for="image">
                                <input type="file" name="image" id="image">
                            </label>
                        </li>
                    </ul>
                </div>
                <button type="submit" class="btn-sm mt-1">Post</button>
            </form>
        </div>
    </div>
</div><!-- add post new box -->
